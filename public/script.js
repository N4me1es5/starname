function generateName() {

    const gender = document.getElementById('gender').value;


    const includeNickname = document.getElementById('includeNickname').checked;


    const namesFile = gender === 'female' ? 'FemaleNames.txt' : 'MaleNames.txt';


    Promise.all([
        fetch(namesFile).then(response => response.text()),
        includeNickname ? fetch('Nicknames.txt').then(response => response.text()) : Promise.resolve(''), 
        fetch('Surnames.txt').then(response => response.text())
    ])
    .then(([namesData, nicknamesData, surnamesData]) => {

        const names = namesData.trim().split('\n');
        const nicknames = includeNickname ? nicknamesData.trim().split('\n') : [];
        const surnames = surnamesData.trim().split('\n');


        const randomName = names[Math.floor(Math.random() * names.length)].trim();
        const randomNickname = includeNickname ? nicknames[Math.floor(Math.random() * nicknames.length)].trim() : ''; 
        const randomSurname = surnames[Math.floor(Math.random() * surnames.length)].trim();


        const fullName = includeNickname ? `${randomName} "${randomNickname}" ${randomSurname}` : `${randomName} ${randomSurname}`;


        const nameDisplay = document.getElementById('nameDisplay');
        nameDisplay.innerHTML = `<p>${fullName}</p>`;
    })
    .catch(error => console.error('Error fetching names:', error));
}
